package com.github.andrdev.senddataapp;

import android.accounts.AccountManager;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.widget.TextView;

import com.google.android.gms.auth.GoogleAuthUtil;
import com.google.android.gms.common.AccountPicker;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;


public class MainActivity extends ActionBarActivity {

    static final int REQUEST_CODE_EMAIL = 2334;

    @InjectView(R.id.reqText)
    TextView mRequestText;

    @InjectView(R.id.reqRes)
    TextView mResultText;

    String playAccMail;

    Sender mSender;

    Callback<String> stringCallback = new Callback<String>() {
        @Override
        public void success(String s, Response response) {
            mResultText.setText(s);
        }

        @Override
        public void failure(RetrofitError error) {
            mResultText.setText(error.getMessage());
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.inject(this);
        mSender = new Sender(this);
        try {
            Intent intent = AccountPicker.newChooseAccountIntent(null, null,
                    new String[]{GoogleAuthUtil.GOOGLE_ACCOUNT_TYPE}, false, null, null, null, null);
            startActivityForResult(intent, MainActivity.REQUEST_CODE_EMAIL);
        } catch (ActivityNotFoundException e) {
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CODE_EMAIL && resultCode == RESULT_OK) {
            playAccMail = data.getStringExtra(AccountManager.KEY_ACCOUNT_NAME);
        }
    }

    @OnClick(R.id.clickMe)
    void sendRequest() {
        mSender.populateCollectedData(playAccMail);
        mSender.sendData(stringCallback, mRequestText);
    }
}

