package com.github.andrdev.senddataapp.model;



public class CollectedData {


    private AccountInfo accountInfo;

    private InstallInfo installInfo;

    private LocationInfo locationInfo;

    public void setInstallInfo(InstallInfo installInfo) {
        this.installInfo = installInfo;
    }

    public void setAccountInfo(AccountInfo accountInfo) {
        this.accountInfo = accountInfo;
    }

    public void setLocationInfo(LocationInfo locationInfo) {
        this.locationInfo = locationInfo;
    }

    @Override
    public String toString() {
        return "CollectedData{" +
                "accountInfo=" + accountInfo +
                ", installInfo=" + installInfo +
                ", locationInfo=" + locationInfo +
                '}';
    }
}
