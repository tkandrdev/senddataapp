package com.github.andrdev.senddataapp.model;


import android.content.Context;
import android.location.Location;
import android.location.LocationManager;

import java.util.List;

public class LocationInfoHarve {

    private Context mContext;

    public LocationInfoHarve(Context ctxt) {
        mContext = ctxt;
    }

    public LocationInfo getLocationInfo() {
        Location location = getLastLocation();
        LocationInfo locationInfo = new LocationInfo();
        if(location != null) {
            locationInfo.setLatitude(String.valueOf(location.getLatitude()));
            locationInfo.setLongitude(String.valueOf(location.getLongitude()));
            locationInfo.setLocationTime(String.valueOf(location.getTime()));
        }
        return locationInfo;
    }

    private Location getLastLocation() {
        LocationManager lm = (LocationManager) mContext.getSystemService(Context.LOCATION_SERVICE);
        List<String> providers = lm.getProviders(true);

        Location location = null;

        for (int i = providers.size() - 1; i >= 0; i--) {
            location = lm.getLastKnownLocation(providers.get(i));
            if (location != null) break;
        }
        return location;
    }
}
