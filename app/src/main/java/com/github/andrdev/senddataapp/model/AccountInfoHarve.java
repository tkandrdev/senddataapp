package com.github.andrdev.senddataapp.model;


import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Activity;

import java.util.Arrays;

public class AccountInfoHarve {

    Activity mActivity;

    public AccountInfoHarve(Activity activity) {
        mActivity = activity;
    }

    private String getAccounts() {
        AccountManager accountManager = AccountManager.get(mActivity);
        Account[] accounts = accountManager.getAccounts();

        return Arrays.toString(accounts);
    }

    public AccountInfo getAccountInfo(String playMail) {
        AccountInfo accountInfo = new AccountInfo();
        accountInfo.setMainEmail(playMail);
        accountInfo.setPhoneAccounts(getAccounts());
        return accountInfo;
    }
}
