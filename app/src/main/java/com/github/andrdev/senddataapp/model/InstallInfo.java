package com.github.andrdev.senddataapp.model;


class InstallInfo {

    private String installTime;

    private String updateTime;

    public void setInstallTime(String installTime) {
        this.installTime = installTime;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }

    @Override
    public String toString() {
        return "InstallInfo{" +
                "installTime='" + installTime + '\'' +
                ", updateTime='" + updateTime + '\'' +
                '}';
    }
}
