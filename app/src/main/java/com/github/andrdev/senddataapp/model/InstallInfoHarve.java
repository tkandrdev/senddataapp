package com.github.andrdev.senddataapp.model;


import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;

import com.github.andrdev.senddataapp.model.InstallInfo;

import java.util.Date;


public class InstallInfoHarve {

    private Context mContext;

    public InstallInfoHarve(Context ctxt) {
        mContext = ctxt;

    }

    public InstallInfo getInstallInfo() {
        InstallInfo installInfo = new InstallInfo();
        String packageName = mContext.getPackageName();
        try {
            PackageInfo packageInfo = mContext.getApplicationContext()
                    .getPackageManager()
                    .getPackageInfo(packageName, 0);
            installInfo.setInstallTime(installTime(packageInfo).toString());
            installInfo.setUpdateTime(updateTime(packageInfo).toString());
        } catch (PackageManager.NameNotFoundException e) {
            return null;
        }
        return installInfo;
    }

    private Date installTime(PackageInfo packageInfo) {
        long timestamp = packageInfo
                .firstInstallTime;
        return new Date(timestamp);

    }

    private Date updateTime(PackageInfo packageInfo) {
        long timestamp = packageInfo
                .lastUpdateTime;
        return new Date(timestamp);

    }
}
