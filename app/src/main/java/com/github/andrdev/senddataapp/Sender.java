package com.github.andrdev.senddataapp;

import android.util.Base64;
import android.widget.TextView;

import com.github.andrdev.senddataapp.model.AccountInfoHarve;
import com.github.andrdev.senddataapp.model.CollectedData;
import com.github.andrdev.senddataapp.model.InstallInfoHarve;
import com.github.andrdev.senddataapp.model.LocationInfoHarve;
import com.google.gson.Gson;

import java.io.UnsupportedEncodingException;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.converter.GsonConverter;


public class Sender {

    private final static String URL = "http://bas.ta/";
    MainActivity mActivity;
    CollectedData collectedData;

    Sender(MainActivity activity) {
        mActivity = activity;
        collectedData = new CollectedData();
    }

    void sendData(Callback<String> callback, TextView requestText) {
        Gson gson = new Gson();
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setLogLevel(RestAdapter.LogLevel.BASIC)
                .setConverter(new GsonConverter(gson))
                .setEndpoint(URL)
                .build();


        String json = gson.toJson(collectedData);
        requestText.setText(URL + json);
        SendDataRetrofitService sendDataRetrofitService = restAdapter.create(SendDataRetrofitService.class);
        sendDataRetrofitService.sendData(base64UrlDecode(json), callback);
    }

    void populateCollectedData(String playMail) {
        setLocationInfo();
        setPackageInstallInfo();
        setAccountInfo(playMail);
    }

    private void setLocationInfo() {
        LocationInfoHarve harvester = new LocationInfoHarve(mActivity);
        collectedData.setLocationInfo(harvester.getLocationInfo());
    }

    private void setAccountInfo(String playMail) {
        AccountInfoHarve accountInfoHarve = new AccountInfoHarve(mActivity);
        collectedData.setAccountInfo(accountInfoHarve.getAccountInfo(playMail));
    }

    private void setPackageInstallInfo() {
        InstallInfoHarve installInfoHarve = new InstallInfoHarve(mActivity);
        collectedData.setInstallInfo(installInfoHarve.getInstallInfo());
    }

    public static String base64UrlDecode(String input) {
        String result = null;
        try {
            byte[] decodedBytes = input.getBytes("UTF-8");
            result = Base64.encodeToString(decodedBytes, Base64.URL_SAFE);
//            byte[] data = Base64.decode(result, Base64.URL_SAFE);
//            result = new String(data, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return result;
    }


}
