package com.github.andrdev.senddataapp.model;


class LocationInfo {

    private String latitude;

    private String longitude;

    private String locationTime;

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public void setLocationTime(String locationTime) {
        this.locationTime = locationTime;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    @Override
    public String toString() {
        return "LocationInfo{" +
                "latitude='" + latitude + '\'' +
                ", longitude='" + longitude + '\'' +
                ", locationTime='" + locationTime + '\'' +
                '}';
    }
}
