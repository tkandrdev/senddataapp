package com.github.andrdev.senddataapp;


import java.util.List;

import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.GET;
import retrofit.http.Path;

interface SendDataRetrofitService {

    @GET("/{repos}")
    void sendData(@Path("repos") String user, Callback<String> callback);

}
