package com.github.andrdev.senddataapp.model;


class AccountInfo {
    private String phoneAccounts;

    private String mainEmail;

    public void setMainEmail(String mainEmail) {
        this.mainEmail = mainEmail;
    }

    public void setPhoneAccounts(String phoneAccounts) {
        this.phoneAccounts = phoneAccounts;
    }

    @Override
    public String toString() {
        return "AccountInfo{" +
                "mainEmail='" + mainEmail + '\'' +
                ", phoneAccounts='" + phoneAccounts + '\'' +
                '}';
    }
}
